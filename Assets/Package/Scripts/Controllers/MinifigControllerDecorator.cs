﻿using Unity.LEGO.Minifig;
using UnityEngine;

namespace Package.Scripts.Controllers
{
    [RequireComponent(typeof(MinifigController))]
    public abstract class MinifigControllerDecorator : MonoBehaviour
    {
        private MinifigController _minifigController;

        public MinifigController MinifigController => _minifigController;

        protected void Awake()
        {
            if(!TryGetComponent<MinifigController>(out _minifigController))
                enabled = false;
        }
    }
}
