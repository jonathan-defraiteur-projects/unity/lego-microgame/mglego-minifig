﻿using System;
using System.Linq;
using Unity.LEGO.Minifig;
using UnityEngine;

namespace Package.Scripts.Controllers
{
    class MinifigPatrolDecorator : MinifigControllerDecorator
    {
        [Serializable]
        struct PatrolPoint
        {
            public Vector3 position;
            public bool animate;
            public MinifigController.SpecialAnimation animation;
            public float wait;
        }
        
        private const float GizmosRadius = .5f;

        [SerializeField] private PatrolPoint[] _patrolPoints = Array.Empty<PatrolPoint>();
        private int currentIndex = -1;

        private void OnDrawGizmosSelected()
        {
            Color gc = Gizmos.color;
            Gizmos.color = Color.red;

            Vector3[] patrolPointPositions = _patrolPoints.Select(_point => _point.position).ToArray();

            Vector3? previous = null;
            foreach (Vector3 pointPosition in patrolPointPositions) {
                Gizmos.DrawSphere(pointPosition, GizmosRadius);
                if (previous.HasValue)
                    Gizmos.DrawLine(previous.Value, pointPosition);
                previous = pointPosition;
            }

            Gizmos.color = gc;
        }

        private void Start()
        {
            MoveToFirstPoint();
        }

        private void MoveToFirstPoint()
        {
            if (0 == _patrolPoints.Length)
                return;
            currentIndex = 0;
            MoveTo(
                GetCurrentPoint().position,
                () => MoveToNext(0), 
                .7f);
        }

        private void MoveToNext(float delay = 0f)
        {
            currentIndex = GetNextIndex();
            Move(delay);
        }

        private void HandleNext()
        {
            PatrolPoint point = GetCurrentPoint();
            if (point.animate)
                MinifigController.PlaySpecialAnimation(point.animation, null, _ => MoveToNext(point.wait));
            else
                MoveToNext(point.wait);
        }

        private void Move(float delay)
        {
            if (_patrolPoints.Length <= currentIndex)
                return;
            MoveTo(
                GetCurrentPoint().position, 
                HandleNext,   
                .3f,
                delay);
        }

        private void MoveTo(Vector3 destination, Action onComplete = null, float speedMultiplier = 1f, float moveDelay = 0f)
        {
            MinifigController.MoveTo(
                destination, 
                .5f, 
                onComplete, 
                0f, 
                moveDelay, 
                true, 
                speedMultiplier, 
                3f, 
                null);
        }

        private PatrolPoint GetCurrentPoint()
        {
            return _patrolPoints[currentIndex];
        }

        private PatrolPoint GetNextPoint()
        {
            return _patrolPoints[GetNextIndex()];
        }

        private int GetNextIndex()
        {
            int next = currentIndex + 1;
            if (next >= _patrolPoints.Length)
                next = 0;
            return next;
        }
    }
}