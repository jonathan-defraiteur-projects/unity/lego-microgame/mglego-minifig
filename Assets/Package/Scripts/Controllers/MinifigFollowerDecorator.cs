﻿using System;
using UnityEngine;

namespace Package.Scripts.Controllers
{
    class MinifigFollowerDecorator : MinifigControllerDecorator
    {
        [SerializeField] private Transform _target;
        [SerializeField] private float _minDistance = 1f;
        [SerializeField] private float _speedMultiplier = 1f;
        [SerializeField] private float _followDelay = .5f;

        private void Start()
        {
            MinifigController.Follow(_target, _minDistance, null, float.PositiveInfinity, _followDelay, true, _speedMultiplier);
        }
    }
}