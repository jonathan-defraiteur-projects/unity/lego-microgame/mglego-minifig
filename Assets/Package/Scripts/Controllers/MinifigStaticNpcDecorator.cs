﻿using System;
using System.Collections;
using Unity.LEGO.Minifig;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Package.Scripts.Controllers
{
    public class MinifigStaticNpcDecorator : MinifigControllerDecorator
    {
        [SerializeField]
        private MinifigController.SpecialAnimation[] _specialAnimations = Array.Empty<MinifigController.SpecialAnimation>();
        [SerializeField]
        private float minDelay = 3f;
        [SerializeField]
        private float maxDelay = 12f;

        private void Start()
        {
            PlayAnimationInRandomTime();
        }

        [ContextMenu("Play Random Animation")]
        public void PlayRandomAnimation()
        {
            PlayAnimationIn(0);
        }

        private void PlayAnimationInRandomTime()
        {
            PlayAnimationIn(GetRandomDelay());
        }

        private float GetRandomDelay()
        {
            if (Math.Abs(minDelay - maxDelay) < .1f)
                return minDelay;
            return Random.Range(minDelay, maxDelay);
        }

        private void PlayAnimationIn(float delay)
        {
            StartCoroutine(PlayAnimationCoroutine(delay));
        }

        private IEnumerator PlayAnimationCoroutine(float delay)
        {
            yield return new WaitForSeconds(delay);
            MinifigController.PlaySpecialAnimation(GetRandomAnimation(), null, OnSpecialComplete);
        }

        private void OnSpecialComplete(bool _value)
        {
            PlayAnimationInRandomTime();
        }

        private MinifigController.SpecialAnimation GetRandomAnimation()
        {
            if (0 == _specialAnimations.Length) {
                throw new IndexOutOfRangeException("No special animation to get.");
            }
            int index = Random.Range(0, _specialAnimations.Length - 1);
            return _specialAnimations[index];
        }
    }
}