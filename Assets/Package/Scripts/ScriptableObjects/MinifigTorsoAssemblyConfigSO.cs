﻿using UnityEngine;

namespace Package.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "MinifigTorsoAssemblyConfig", menuName = "mgLEGO/Minifig/Torso Assembly Config", order = 1)]
    public class MinifigTorsoAssemblyConfigSO : MinifigPartConfigSO
    {
        [Header("Torso")]
        [SerializeField] private Material torsoMain;
        [SerializeField] private Material torsoFront;
        [SerializeField] private Material torsoBack;
        [Header("Arms")]
        [SerializeField] private bool armsColorSync = false;
        [SerializeField] private Material armLeft;
        [SerializeField] private Material handLeft;
        [SerializeField] private Material armRight;
        [SerializeField] private Material handRight;

        public bool IsEmpty()
        {
            return null == torsoMain
                   && null == torsoFront
                   && null == torsoBack
                   && null == armLeft
                   && null == handLeft
                   && null == armRight
                   && null == handRight;
        }
    
        public Material TorsoMain
        {
            get => torsoMain;
            set => torsoMain = value;
        }

        public Material TorsoFront
        {
            get => torsoFront;
            set => torsoFront = value;
        }

        public Material TorsoBack
        {
            get => torsoBack;
            set => torsoBack = value;
        }

        public bool ArmsColorSync
        {
            get => armsColorSync;
            set => armsColorSync = value;
        }

        public Material ArmLeft
        {
            get => armLeft;
            set => armLeft = value;
        }

        public Material HandLeft
        {
            get => handLeft;
            set => handLeft = value;
        }

        public Material ArmRight
        {
            get => armRight;
            set => armRight = value;
        }

        public Material HandRight
        {
            get => handRight;
            set => handRight = value;
        }
    }
}