﻿using UnityEngine;

namespace Package.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "MinifigHeadConfig", menuName = "mgLEGO/Minifig/Head Config", order = 1)]
    public class MinifigHeadConfigSO : MinifigPartConfigSO
    {
        [Header("Head")]
        [SerializeField] public Material head;
        [SerializeField] public Material face;

        public bool IsEmpty()
        {
            return null == head && null == face;
        }
    
        public Material Head
        {
            get => head;
            set => head = value;
        }

        public Material Face
        {
            get => face;
            set => face = value;
        }
    }
}