﻿using System.Collections.Generic;
using UnityEngine;

namespace Package.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "MinifigHatConfig", menuName = "mgLEGO/Minifig/Hat", order = 1)]
    public class MinifigHatSO : MinifigPartConfigSO
    {
        [System.Serializable]
        public struct Shell
        {
            public Mesh mesh;
            public Material material;
        }

        [SerializeField] private List<Shell> shells;

        public bool IsEmpty()
        {
            return 0 == shells.Count;
        }

        public List<Shell> Shells
        {
            get => shells;
            set => shells = value;
        }
    }
}
