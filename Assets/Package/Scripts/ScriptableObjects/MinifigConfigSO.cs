﻿using UnityEditor;
using UnityEngine;

namespace Package.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "MinifigConfig", menuName = "mgLEGO/Minifig/Config", order = 1)]
    public class MinifigConfigSO : MinifigPartConfigSO
    {
        [ContextMenuItem("Create and fill", "CreateAndFillHatSO")]
        [SerializeField] private MinifigHatSO hat;
        [ContextMenuItem("Create and fill", "CreateAndFillHeadConfigSO")]
        [SerializeField] private MinifigHeadConfigSO head;
        [ContextMenuItem("Create and fill", "CreateAndFillTorsoAssemblyConfigSO")]
        [SerializeField] private MinifigTorsoAssemblyConfigSO torso;
        [ContextMenuItem("Create and fill", "CreateAndFillLegsConfigSO")]
        [SerializeField] private MinifigLegsConfigSO legs;

        public bool IsEmpty()
        {
            return hat && hat.IsEmpty()
                       && head && head.IsEmpty()
                       && torso && torso.IsEmpty()
                       && legs && legs.IsEmpty();
        }

        public MinifigHatSO Hat => hat;
        public MinifigHeadConfigSO Head => head;
        public MinifigTorsoAssemblyConfigSO Torso => torso;
        public MinifigLegsConfigSO Legs => legs;

        [MenuItem("CONTEXT/MinifigConfigSO/Create children")]
        static void CreateChildren(MenuCommand _command)
        {
            MinifigConfigSO source = (MinifigConfigSO) _command.context;
            if (null == source.hat)
                source.CreateAndFillHatSO();
            if (null == source.head)
                source.CreateAndFillHeadConfigSO();
            if (null == source.torso)
                source.CreateAndFillTorsoAssemblyConfigSO();
            if (null == source.legs)
                source.CreateAndFillLegsConfigSO();
        }

        void CreateAndFillHatSO()
        {
            hat = (new MinifigConfigGenerator(this))
                .GenerateSOAsset<MinifigHatSO>();
        }

        void CreateAndFillHeadConfigSO()
        {
            head = (new MinifigConfigGenerator(this))
                .GenerateSOAsset<MinifigHeadConfigSO>();
        }

        void CreateAndFillTorsoAssemblyConfigSO()
        {
            torso = (new MinifigConfigGenerator(this))
                .GenerateSOAsset<MinifigTorsoAssemblyConfigSO>();
        }

        void CreateAndFillLegsConfigSO()
        {
            legs = (new MinifigConfigGenerator(this))
                .GenerateSOAsset<MinifigLegsConfigSO>();
        }
    }
}