﻿using UnityEditor;
using UnityEngine;

namespace Package.Scripts.ScriptableObjects.Editor
{
    [CustomEditor(typeof(MinifigHatSO))]
    public class MinifigHatConfigSOEditor: MinifigSOEditor<MinifigHatSO>
    {
        protected override PreviewRenderUtility GetPRU()
        {
            var pru = base.GetPRU();
            pru.camera.transform.position = new Vector3(0, 0f, -14f);
            pru.cameraFieldOfView = 10;
            return pru;
        }
        
        protected override Material MaterialFromConfig(MinifigHatSO config, SkinnedMeshRenderer mesh)
        {
            throw new System.NotImplementedException();
        }

        protected override void DrawMeshes()
        {
            foreach (MinifigHatSO.Shell shell in MinifigConfigTarget.Shells) {
                _previewRenderUtility.DrawMesh(shell.mesh, GetRectoMatrix(), shell.material, 0);
                _previewRenderUtility.DrawMesh(shell.mesh, GetVersoMatrix(), shell.material, 0);
            }
        }
    
        protected override Matrix4x4 GetRectoMatrix()
        {
            return Matrix4x4.TRS(
                new Vector3(-.25f, -.25f, 1f), 
                Quaternion.Euler(0,140,0),
                Vector3.one);
        }
    
        protected override Matrix4x4 GetVersoMatrix()
        {
            return Matrix4x4.TRS(
                new Vector3(.75f, .75f, 5f),
                Quaternion.Euler(0, -40, 0),
                Vector3.one
                );
        }
    }
}