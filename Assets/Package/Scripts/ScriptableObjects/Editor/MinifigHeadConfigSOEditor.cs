﻿using UnityEditor;
using UnityEngine;

namespace Package.Scripts.ScriptableObjects.Editor
{
    [CustomEditor(typeof(MinifigHeadConfigSO))]
    public class MinifigHeadConfigSOEditor: MinifigSOEditor<MinifigHeadConfigSO>
    {
        protected override PreviewRenderUtility GetPRU()
        {
            var pru = base.GetPRU();
            pru.camera.transform.position = new Vector3(0, 3.43f, -8);
            pru.cameraFieldOfView = 10;
            return pru;
        }
    
        protected override Material MaterialFromConfig(MinifigHeadConfigSO config, SkinnedMeshRenderer mesh)
        {
            Material noMat = mesh.sharedMaterial;
            if (null == config) return noMat;
        
            switch (mesh.name) {
                case "Head":
                    return null != config.Head ? config.Head : noMat;
                case "Face":
                    return null != config.Face ? config.Face : noMat;
                case "Torso_Front":
                case "Torso_Back":
                case "Torso_main":
                case "Arm_L_Front":
                case "Arm_L_Main":
                case "Arm_R_Front":
                case "Arm_R_Main":
                case "Hand_Left":
                case "Hand_Right":
                    return null;
            }
            return mesh.sharedMaterial;
        }
    }
}