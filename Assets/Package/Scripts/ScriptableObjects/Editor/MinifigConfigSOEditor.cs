﻿using UnityEditor;
using UnityEngine;

namespace Package.Scripts.ScriptableObjects.Editor
{
    [CustomEditor(typeof(MinifigConfigSO))]
    public class MinifigConfigSOEditor: MinifigSOEditor<MinifigConfigSO>
    {
        protected override void DrawMeshes()
        {
            base.DrawMeshes();
            
            if (null == MinifigConfigTarget.Hat ||null == MinifigConfigTarget.Hat.Shells)
                return;
            foreach (MinifigHatSO.Shell shell in MinifigConfigTarget.Hat.Shells) {
                _previewRenderUtility.DrawMesh(shell.mesh, GetHatRectoMatrix(), shell.material, 0);
                _previewRenderUtility.DrawMesh(shell.mesh, GetHatVersoMatrix(), shell.material, 0);
            }
        }

        private Matrix4x4 GetHatRectoMatrix()
        {
            return Matrix4x4.TRS(
                new Vector3(0, 3.85f, 0),
                Quaternion.Euler(0, 180, 0), 
                Vector3.one);
        }

        private Matrix4x4 GetHatVersoMatrix()
        {
            return Matrix4x4.TRS(
                new Vector3(2, 3.85f, 2),
                Quaternion.identity, 
                Vector3.one);
        }

        protected override Material MaterialFromConfig(MinifigConfigSO config, SkinnedMeshRenderer mesh)
        {
            Material noMat = mesh.sharedMaterial;
            if (null == config) return noMat;
        
            switch (mesh.name) {
                case "Head":
                    return null != config.Head ? config.Head.Head : noMat;
                case "Face":
                    return null != config.Head ? config.Head.Face : noMat;
                case "Torso_Front":
                    return null != config.Torso ? config.Torso.TorsoFront : noMat;
                case "Torso_Back":
                    return null != config.Torso ? config.Torso.TorsoBack : noMat;
                case "Torso_main":
                    return null != config.Torso ? config.Torso.TorsoMain : noMat;
                case "Arm_L_Front":
                    return null != config.Torso ? config.Torso.ArmLeft : noMat;
                case "Arm_L_Main":
                    return null != config.Torso ? config.Torso.ArmLeft : noMat;
                case "Arm_R_Front":
                    return null != config.Torso ? config.Torso.ArmRight : noMat;
                case "Arm_R_Main":
                    return null != config.Torso ? config.Torso.ArmRight : noMat;
                case "Hand_Left":
                    return null != config.Torso ? config.Torso.HandLeft : noMat;
                case "Hand_Right":
                    return null != config.Torso ? config.Torso.HandRight : noMat;
                case "Hip_Crotch":
                    return null != config.Legs ? config.Legs.HipCrotch : noMat;
                case "Hip_Front":
                    return null != config.Legs ? config.Legs.HipFront : noMat;
                case "Hip_Main":
                    return null != config.Legs ? config.Legs.HipMain : noMat;
                case "Leg_L_Foot":
                    return null != config.Legs ? config.Legs.LegLeftFoot : noMat;
                case "Leg_L_Front":
                    return null != config.Legs ? config.Legs.LegLeftFront : noMat;
                case "Leg_L_Main":
                    return null != config.Legs ? config.Legs.LegLeftMain : noMat;
                case "Leg_L_Side":
                    return null != config.Legs ? config.Legs.LegLeftSide : noMat;
                case "Leg_R_Foot":
                    return null != config.Legs ? config.Legs.LegRightFoot : noMat;
                case "Leg_R_Front":
                    return null != config.Legs ? config.Legs.LegRightFront : noMat;
                case "Leg_R_Main":
                    return null != config.Legs ? config.Legs.LegRightMain : noMat;
                case "Leg_R_Side":
                    return null != config.Legs ? config.Legs.LegRightSide : noMat;
            }
            return noMat;
        }
    }
}
