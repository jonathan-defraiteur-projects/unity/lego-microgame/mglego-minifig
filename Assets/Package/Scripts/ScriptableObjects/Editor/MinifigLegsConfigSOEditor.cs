﻿using UnityEditor;
using UnityEngine;

namespace Package.Scripts.ScriptableObjects.Editor
{
    [CustomEditor(typeof(MinifigLegsConfigSO))]
    public class MinifigLegsConfigSOEditor: MinifigSOEditor<MinifigLegsConfigSO>
    {
        protected override PreviewRenderUtility GetPRU()
        {
            var pru = base.GetPRU();
            pru.camera.transform.position = new Vector3(0, 1f, -12.05f);
            pru.cameraFieldOfView = 10;
            return pru;
        }
    
        protected override Matrix4x4 GetVersoMatrix()
        {
            return Matrix4x4.Translate(new Vector3(1.5f, 1.5f, 800));
        }
    
        protected override Material MaterialFromConfig(MinifigLegsConfigSO config, SkinnedMeshRenderer mesh)
        {
            Material noMat = mesh.sharedMaterial;
            if (null == config) return noMat;
        
            switch (mesh.name) {
                case "Hip_Crotch":
                    return null != config.HipCrotch ? config.HipCrotch : noMat;
                case "Hip_Front":
                    return null != config.HipFront ? config.HipFront : noMat;
                case "Hip_Main":
                    return null != config.HipMain ? config.HipMain : noMat;
                case "Leg_L_Foot":
                    return null != config.LegLeftFoot ? config.LegLeftFoot : noMat;
                case "Leg_L_Front":
                    return null != config.LegLeftFront ? config.LegLeftFront : noMat;
                case "Leg_L_Main":
                    return null != config.LegLeftMain ? config.LegLeftMain : noMat;
                case "Leg_L_Side":
                    return null != config.LegLeftSide ? config.LegLeftSide : noMat;
                case "Leg_R_Foot":
                    return null != config.LegRightFoot ? config.LegRightFoot : noMat;
                case "Leg_R_Front":
                    return null != config.LegRightFront ? config.LegRightFront : noMat;
                case "Leg_R_Main":
                    return null != config.LegRightMain ? config.LegRightMain : noMat;
                case "Leg_R_Side":
                    return null != config.LegRightSide ? config.LegRightSide : noMat;
            
                case "Torso_Front":
                case "Torso_Back":
                case "Torso_main":
                case "Arm_L_Front":
                case "Arm_L_Main":
                case "Arm_R_Front":
                case "Arm_R_Main":
                case "Hand_Left":
                case "Hand_Right":
                    return null;
            }
            return noMat;
        }
    }
}