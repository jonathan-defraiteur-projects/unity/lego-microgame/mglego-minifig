﻿using UnityEditor;
using UnityEngine;

namespace Package.Scripts.ScriptableObjects.Editor
{
    [CustomEditor(typeof(MinifigTorsoAssemblyConfigSO))]
    public class MinifigTorsoConfigSOEditor: MinifigSOEditor<MinifigTorsoAssemblyConfigSO>
    {
        protected override PreviewRenderUtility GetPRU()
        {
            var pru = base.GetPRU();
            pru.camera.transform.position = new Vector3(.1f, 2.8f, -10);
            pru.cameraFieldOfView = 15;
            return pru;
        }
    
        protected override Matrix4x4 GetVersoMatrix()
        {
            return Matrix4x4.Translate(new Vector3(1.5f, 1.5f, 8));
        }
    
        protected override Material MaterialFromConfig(MinifigTorsoAssemblyConfigSO config, SkinnedMeshRenderer mesh)
        {
            Material noMat = mesh.sharedMaterial;
            if (null == config) return noMat;
        
            switch (mesh.name) {
                case "Torso_Front":
                    return null != config.TorsoFront ? config.TorsoFront : noMat;
                case "Torso_Back":
                    return null != config.TorsoBack ? config.TorsoBack : noMat;
                case "Torso_main":
                    return null != config.TorsoMain ? config.TorsoMain : noMat;
                case "Arm_L_Front":
                    return null != config.ArmLeft ? config.ArmLeft : noMat;
                case "Arm_L_Main":
                    return null != config.ArmLeft ? config.ArmLeft : noMat;
                case "Arm_R_Front":
                    return null != config.ArmRight ? config.ArmRight : noMat;
                case "Arm_R_Main":
                    return null != config.ArmRight ? config.ArmRight : noMat;
                case "Hand_Left":
                    return null != config.HandLeft ? config.HandLeft : noMat;
                case "Hand_Right":
                    return null != config.HandRight ? config.HandRight : noMat;
                case "Head":
                case "Face":
                case "Hip_Crotch":
                case "Hip_Front":
                case "Hip_Main":
                case "Leg_L_Foot":
                case "Leg_L_Front":
                case "Leg_L_Main":
                case "Leg_L_Side":
                case "Leg_R_Foot":
                case "Leg_R_Front":
                case "Leg_R_Main":
                case "Leg_R_Side":
                    return null;
            }
            return noMat;
        }
    }
}