﻿using UnityEditor;
using UnityEngine;

//[CustomPropertyDrawer(typeof(MinifigHeadConfigSO))]
namespace Package.Scripts.ScriptableObjects.Editor
{
    public class MinifigHeadConfigDrawer : PropertyDrawer
    {
        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var amountRect = new Rect(position.x, position.y, 60, position.height);
            var unitRect = new Rect(position.x + 65, position.y, 50, position.height);

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            //var face = property.serializedObject.FindProperty("face");
            var head = property.serializedObject.FindProperty("head");
            var face = head.serializedObject.FindProperty("face");
            EditorGUI.PropertyField(amountRect, face, GUIContent.none);
            EditorGUI.PropertyField(unitRect, head, GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}