﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Package.Scripts.ScriptableObjects.Editor
{
    public abstract class MinifigSOEditor<MinifigConfig>: UnityEditor.Editor where MinifigConfig: MinifigPartConfigSO
    {
        //We dont create a PRU every frame, keep a reference to your one!
        protected PreviewRenderUtility _previewRenderUtility;
        protected static SkinnedMeshRenderer[] _skinnedMeshes;
        protected MinifigConfig _minifigConfig;

        protected bool IsPreviewRenderUtilityMissing => null == _previewRenderUtility;
        protected bool IsMeshesMissing => null == _skinnedMeshes || 0 == _skinnedMeshes.Length;
        protected MinifigConfig MinifigConfigTarget
        {
            get
            {
                if (null == _minifigConfig)
                    _minifigConfig = target as MinifigConfig;
                return _minifigConfig;
            }
        }

        protected virtual PreviewRenderUtility GetPRU()
        {
            if (IsPreviewRenderUtilityMissing)
                BuildPreviewRenderUtility();
            return _previewRenderUtility;
        }

        protected virtual void BuildPreviewRenderUtility()
        {
            _previewRenderUtility = new PreviewRenderUtility();
            //We set the previews camera to 6 units back, look towards the middle of the 'scene'
            _previewRenderUtility.camera.farClipPlane = 20;
            _previewRenderUtility.camera.transform.position = new Vector3(.75f, 2, -17);
            _previewRenderUtility.camera.transform.rotation = Quaternion.identity;
            _previewRenderUtility.lights[0].transform.rotation = Quaternion.Euler(45, 45, 0);
            _previewRenderUtility.lights[0].color = Color.white;
            _previewRenderUtility.lights[0].intensity = .75f;
            _previewRenderUtility.lights[1].transform.position = new Vector3(0, 5, -17);
            _previewRenderUtility.lights[1].color = Color.black;
            _previewRenderUtility.ambientColor = Color.white * .5f;
        }

        protected SkinnedMeshRenderer[] GetMeshes()
        {
            if (IsMeshesMissing)
                FetchMeshes();
            return _skinnedMeshes;
        }

        protected virtual void FetchMeshes()
        {
            _skinnedMeshes = Array.Empty<SkinnedMeshRenderer>();
            
            string[] guids = AssetDatabase.FindAssets("Minifig t:Model");
            string path = null;
            foreach (string guid in guids) {
                string tmpPath = AssetDatabase.GUIDToAssetPath(guid);
                if (Regex.IsMatch(tmpPath, @"Minifig.fbx$")) {
                    path = tmpPath;
                    break;
                }
            }

            if (null == path) return;
            var model = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            if (null == model) return;
            _skinnedMeshes = model.GetComponentsInChildren<SkinnedMeshRenderer>();
        }
    
        // Fetch all the relevant information
        private bool ValidateData()
        {
            GetPRU();
            GetMeshes();
            return !(IsPreviewRenderUtilityMissing || IsMeshesMissing);
        }
        
        // This is always called before OnPreviewGUI
        public override bool HasPreviewGUI()
        {
            return ValidateData();
        }

        public override Texture2D RenderStaticPreview(string assetPath, Object[] subAssets, int width, int height)
        {
            return PreparePreviewPreview(new Rect(0, 0, width, height), GUIStyle.none)
                .EndStaticPreview();
        }
    
        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            //Only render our 3D 'preview' when the UI is 'repainting'.
            //The OnPreviewGUI, like other GUI methods, will be called LOTS
            //of times ever frame to handle different events.
            //We only need to Render our preview once when the GUI is being repainted!
            if (IsNotRepaintingEvent())
                return;
        
            //Mesh renderer missing?
            if(GetMeshes().Length == 0)
            {
                //EditorGUI.DropShadowLabel is used often in these preview areas - it 'fits' well.
                EditorGUI.DropShadowLabel(r, "Mesh Renderer Required");
                return;
            }

            Texture resultRender = PreparePreviewPreview(r, background).EndPreview();
        
            //If we omit the line bellow, then you wouldnt actually see anything in the preview!
            //The preview image is generated, but that was all done in our 'virtual' PreviewRenderUtility 'scene'.
            //We still need to draw something in the PreviewGUI area..!
        
            //So we draw the image that was generated into the preview GUI area, filling the entire area with this image.
            GUI.DrawTexture(r, resultRender, ScaleMode.StretchToFill, false);
        }

        protected bool IsNotRepaintingEvent()
        {
            return Event.current.type != EventType.Repaint;
        }
    
        public PreviewRenderUtility PreparePreviewPreview(Rect r, GUIStyle background)
        {
            GetPRU();
            GetMeshes();
            
            if (IsPreviewRenderUtilityMissing)
                throw new Exception($"No PreviewRenderUtility to render {MinifigConfigTarget.name}.");
            if (IsMeshesMissing)
                throw new Exception($"No mesh for render {MinifigConfigTarget.name}.");

            //Tell the PRU to prepair itself - we pass along the
            //rect of the preview area so the PRU knows what size 
            //of a preview to render.
            _previewRenderUtility.BeginPreview(r, background);

            //We draw our mesh manually - it is not attached to any 'gameobject' in the preview 'scene'.
            //The preview 'scene' only contains a camera and a light. We need to render things manually.
            //We pass along the mesh set on the mesh filter and the material set on the renderer
            //_previewRenderUtility.DrawMesh(_targetMeshFilter.sharedMesh, Matrix4x4.identity, _targetMeshRenderer.sharedMaterial, 0);

            DrawMeshes();
        
            //Tell the camera to actually render the preview.
            _previewRenderUtility.camera.Render();

            //Now that we are done, we can end the preview. This method will spit out a Texture
            //The texture contains the image that was rendered by the preview utillity camera :)
            return _previewRenderUtility;
        }

        protected virtual void DrawMeshes()
        {
            foreach (var skinnedMesh in _skinnedMeshes) {
                Material mat = MaterialFromConfig(MinifigConfigTarget, skinnedMesh);
                if (null != mat) {
                    _previewRenderUtility.DrawMesh(skinnedMesh.sharedMesh, GetRectoMatrix(), mat, 0);
                    _previewRenderUtility.DrawMesh(skinnedMesh.sharedMesh, GetVersoMatrix(), mat, 0);
                }
            }
        }

        protected abstract Material MaterialFromConfig(MinifigConfig config, SkinnedMeshRenderer mesh);

        protected virtual Matrix4x4 GetRectoMatrix()
        {
            return Matrix4x4.Rotate(Quaternion.Euler(0,180,0));
        }
    
        protected virtual Matrix4x4 GetVersoMatrix()
        {
            return Matrix4x4.Translate(new Vector3(2, 0, 2));
        }
    
        void OnDisable()
        {
            //Gotta clean up after yourself!
            GetPRU().Cleanup();
        }
    }
}
