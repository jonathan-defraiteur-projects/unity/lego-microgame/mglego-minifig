﻿using UnityEngine;

namespace Package.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "MinifigLegsConfig", menuName = "mgLEGO/Minifig/Legs Config", order = 1)]
    public class MinifigLegsConfigSO : MinifigPartConfigSO
    {
        [Header("Hip")]
        [SerializeField] private Material hipMain;
        [SerializeField] private Material hipFront;
        [SerializeField] private Material hipCrotch;
        [Header("Legs")]
        [SerializeField] private Material legLeftMain;
        [SerializeField] private Material legLeftFront;
        [SerializeField] private Material legLeftFoot;
        [SerializeField] private Material legLeftSide;
        [SerializeField] private Material legRightMain;
        [SerializeField] private Material legRightFront;
        [SerializeField] private Material legRightFoot;
        [SerializeField] private Material legRightSide;

        public bool IsEmpty()
        {
            return null == hipMain
                   && null == hipFront
                   && null == hipCrotch
                   && null == legLeftMain
                   && null == legLeftFront
                   && null == legLeftFoot
                   && null == legLeftSide
                   && null == legRightMain
                   && null == legRightFront
                   && null == legRightFoot
                   && null == legRightSide;
        }

        public Material HipMain
        {
            get => hipMain;
            set => hipMain = value;
        }

        public Material HipFront
        {
            get => hipFront;
            set => hipFront = value;
        }

        public Material HipCrotch
        {
            get => hipCrotch;
            set => hipCrotch = value;
        }

        public Material LegLeftMain
        {
            get => legLeftMain;
            set => legLeftMain = value;
        }

        public Material LegLeftFront
        {
            get => legLeftFront;
            set => legLeftFront = value;
        }

        public Material LegLeftFoot
        {
            get => legLeftFoot;
            set => legLeftFoot = value;
        }

        public Material LegLeftSide
        {
            get => legLeftSide;
            set => legLeftSide = value;
        }

        public Material LegRightMain
        {
            get => legRightMain;
            set => legRightMain = value;
        }

        public Material LegRightFront
        {
            get => legRightFront;
            set => legRightFront = value;
        }

        public Material LegRightFoot
        {
            get => legRightFoot;
            set => legRightFoot = value;
        }

        public Material LegRightSide
        {
            get => legRightSide;
            set => legRightSide = value;
        }
    }
}