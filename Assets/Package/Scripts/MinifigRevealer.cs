using UnityEngine;

namespace Package.Scripts
{
    public class MinifigRevealer : MonoBehaviour
    {
        private void Reset()
        {
            Reveal();
        }
    
        [ContextMenu("Reveal")]
        public void Reveal()
        {
            RecursiveUnhide(transform);
        }
    
        protected void RecursiveUnhide(Transform _transform)
        {
            _transform.gameObject.hideFlags = HideFlags.None;
            foreach (Transform child in _transform)
                RecursiveUnhide(child);
        }
    }
}
