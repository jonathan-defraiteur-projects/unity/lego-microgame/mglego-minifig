﻿using System.Text.RegularExpressions;
using Package.Scripts.ScriptableObjects;
using UnityEditor;
using UnityEngine;

namespace Package.Scripts
{
    class MinifigConfigGenerator
    {
        private MinifigConfigSO source;

        private string Path => AssetDatabase.GetAssetPath(source);
        private string Directory => System.IO.Path.GetDirectoryName(Path);
        private string FileNameWithoutExtension => System.IO.Path.GetFileNameWithoutExtension(Path);

        public MinifigConfigGenerator(MinifigConfigSO _source)
        {
            source = _source;
        }

        public T GenerateSOAsset<T>() where T : MinifigPartConfigSO
        {
            T hat = ScriptableObject.CreateInstance<T>();
            string path = AssetDatabase.GenerateUniqueAssetPath(GetCompletePath<T>());
            AssetDatabase.CreateAsset(hat, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return hat;
        }
    
        private string GetCompletePath<T>() where T : MinifigPartConfigSO
        {
            string fileName = GetSuggestedFileName<T>();
            return Directory + System.IO.Path.DirectorySeparatorChar + fileName + ".asset";
        }

        private string GetSuggestedFileName<T>() where T : MinifigPartConfigSO
        {
            string typeSuffix = Regex.Replace(typeof(T).Name, "Minifig|Config|SO", "");
            return GetSuggestedRootName() + "_" + typeSuffix;
        }

        private string GetSuggestedRootName()
        {
            string pattern = source.GetType().Name + "|Config|_";
            return Regex.Replace(FileNameWithoutExtension, pattern, "");
        }
    }
}