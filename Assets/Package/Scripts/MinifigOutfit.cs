using System;
using System.Collections.Generic;
using System.Linq;
using Package.Scripts.ScriptableObjects;
using UnityEditor;
using UnityEngine;

namespace Package.Scripts
{
    public class MinifigOutfit : MonoBehaviour
    {
        private struct HatPart
        {
            public GameObject gameObject;
            public MeshFilter meshFilter;
            public MeshRenderer meshRenderer;
        }
        
        public enum OutfitPiece
        {
            Arm_L_Front,
            Arm_L_Main,
            Arm_R_Front,
            Arm_R_Main,
            Face,
            Hand_Left,
            Hand_Right,
            Head,
            Hip_Crotch,
            Hip_Front,
            Hip_Main,
            Leg_L_Foot,
            Leg_L_Front,
            Leg_L_Main,
            Leg_L_Side,
            Leg_R_Foot,
            Leg_R_Front,
            Leg_R_Main,
            Leg_R_Side,
            Torso_Back,
            Torso_Front,
            Torso_main,
        }

        [System.Serializable]
        public class RendererDebug
        {
            public string name;
            public Renderer renderer;
            public Material material;
        }

        [SerializeField] private MinifigConfigSO config;
        [Header("Hat")]
        private Transform hatLoc;
        private List<HatPart> hatParts = new List<HatPart>();
        [SerializeField] private List<MinifigHatSO.Shell> hatShells;
        [Header("Head")]
        [SerializeField] private Material face;
        [SerializeField] private Material head;
        [Header("Top")]
        [SerializeField] private Material torsoMain;
        [SerializeField] private Material torsoFront;
        [SerializeField] private Material torsoBack;
        [SerializeField] private bool armSyncColor = false;
        [SerializeField] private Material armLeft;
        [SerializeField] private Material armRight;
        [SerializeField] private Material handLeft;
        [SerializeField] private Material handRight;
        [Header("Bottom")]
        [SerializeField] private Material hipMain;
        [SerializeField] private Material hipFront;
        [SerializeField] private Material hipCrotch;
        [SerializeField] private Material legLeftFoot;
        [SerializeField] private Material legLeftFront;
        [SerializeField] private Material legLeftMain;
        [SerializeField] private Material legLeftSide;
        [SerializeField] private Material legRightFoot;
        [SerializeField] private Material legRightFront;
        [SerializeField] private Material legRightMain;
        [SerializeField] private Material legRightSide;
        [Header("Debug")]
        [SerializeField] private bool displayInHierarchy = false;
        [SerializeField] private List<RendererDebug> rendererDebugs;
    
        private Dictionary<OutfitPiece, Renderer> renderers;

        private void Reset()
        {
            FetchRenderers();
        
            face = renderers[OutfitPiece.Face]?.sharedMaterial;
            head = renderers[OutfitPiece.Head]?.sharedMaterial;
        
            torsoMain = renderers[OutfitPiece.Torso_main]?.sharedMaterial;
            torsoFront = renderers[OutfitPiece.Torso_Front]?.sharedMaterial;
            torsoBack = renderers[OutfitPiece.Torso_Back]?.sharedMaterial;
            armLeft = renderers[OutfitPiece.Arm_L_Front]?.sharedMaterial;
            armRight = renderers[OutfitPiece.Arm_R_Front]?.sharedMaterial;
            handLeft = renderers[OutfitPiece.Hand_Left]?.sharedMaterial;
            handRight = renderers[OutfitPiece.Hand_Right]?.sharedMaterial;
            armSyncColor = armLeft == armRight && handLeft == handRight;
        
            hipMain = renderers[OutfitPiece.Hip_Main]?.sharedMaterial;
            hipFront = renderers[OutfitPiece.Hip_Front]?.sharedMaterial;
            hipCrotch = renderers[OutfitPiece.Hip_Crotch]?.sharedMaterial;
            legLeftFoot = renderers[OutfitPiece.Leg_L_Foot]?.sharedMaterial;
            legLeftFront = renderers[OutfitPiece.Leg_L_Front]?.sharedMaterial;
            legLeftMain = renderers[OutfitPiece.Leg_L_Main]?.sharedMaterial;
            legLeftSide = renderers[OutfitPiece.Leg_L_Side]?.sharedMaterial;
            legRightFoot = renderers[OutfitPiece.Leg_R_Foot]?.sharedMaterial;
            legRightFront = renderers[OutfitPiece.Leg_R_Front]?.sharedMaterial;
            legRightMain = renderers[OutfitPiece.Leg_R_Main]?.sharedMaterial;
            legRightSide = renderers[OutfitPiece.Leg_R_Side]?.sharedMaterial;
        
            FetchHat();
        }

        void Start()
        {
        
        }

        void Update()
        {
        
        }

        private void OnValidate()
        {
            if (displayInHierarchy)
                RecursiveUnHide(transform);

            FetchRenderers();
        
            // Head
            ApplyMaterial(OutfitPiece.Face, face);
            ApplyMaterial(OutfitPiece.Head, head);
            // Top
            ApplyMaterial(OutfitPiece.Torso_main, torsoMain);
            ApplyMaterial(OutfitPiece.Torso_Front, torsoFront);
            ApplyMaterial(OutfitPiece.Torso_Back, torsoBack);
            if (armLeft) {
                ApplyMaterial(new[]{OutfitPiece.Arm_L_Front, OutfitPiece.Arm_L_Main}, armLeft);
                if (armSyncColor)
                    ApplyMaterial(new[]{OutfitPiece.Arm_R_Front, OutfitPiece.Arm_R_Main}, armLeft);
            }
            if (!armSyncColor && armRight) {
                ApplyMaterial(new[]{OutfitPiece.Arm_R_Front, OutfitPiece.Arm_R_Main}, armRight);
            }
            if (handLeft) {
                ApplyMaterial(OutfitPiece.Hand_Left, handLeft);
                if (armSyncColor)
                    ApplyMaterial(OutfitPiece.Hand_Right, handLeft);
            }
            if (!armSyncColor && handRight) {
                ApplyMaterial(OutfitPiece.Hand_Right, handRight);
            }
            // Bottom
            ApplyMaterial(OutfitPiece.Hip_Main, hipMain);
            ApplyMaterial(OutfitPiece.Hip_Front, hipFront);
            ApplyMaterial(OutfitPiece.Hip_Crotch, hipCrotch);
            ApplyMaterial(OutfitPiece.Leg_L_Foot, legLeftFoot);
            ApplyMaterial(OutfitPiece.Leg_L_Front, legLeftFront);
            ApplyMaterial(OutfitPiece.Leg_L_Main, legLeftMain);
            ApplyMaterial(OutfitPiece.Leg_L_Side, legLeftSide);
            ApplyMaterial(OutfitPiece.Leg_R_Foot, legRightFoot);
            ApplyMaterial(OutfitPiece.Leg_R_Front, legRightFront);
            ApplyMaterial(OutfitPiece.Leg_R_Main, legRightMain);
            ApplyMaterial(OutfitPiece.Leg_R_Side, legRightSide);
        
            if (config && config.IsEmpty())
                UpdateConfig();
        
            if (!HasHatLoc())
                FetchHat();
            else
                ApplyHat();
        }

        private void FetchRenderers()
        {
            rendererDebugs = GetComponentsInChildren<Renderer>()
                .Select(_r => new RendererDebug {
                    name = _r.name,
                    renderer = _r,
                    material = _r.sharedMaterial
                })
                .ToList();
        
            if (renderers == null || renderers.Count == 0) {
                renderers = new Dictionary<OutfitPiece, Renderer>();
                foreach (Renderer renderer in GetComponentsInChildren<Renderer>()) {
                    if (Enum.TryParse(renderer.name, out OutfitPiece enumValue))
                        renderers.Add(enumValue, renderer);
                }
            }
        }

        private void FetchHat()
        {
            FetchHatLoc();
            if (!HasHatLoc())
                return;
            FetchHatParts();
            FetchHatShells();
        }

        private void FetchHatLoc()
        {
            hatLoc = transform.FindDeepChild("hat_loc");
        }

        private void FetchHatParts()
        {
            if (!HasHatLoc())
                return;
            MeshFilter[] filters = hatLoc.GetComponentsInChildren<MeshFilter>();
            hatParts = filters.Select(_filter => new HatPart {
                gameObject = _filter.gameObject,
                meshFilter = _filter,
                meshRenderer = _filter.GetComponent<MeshRenderer>()
            }).ToList();
        }

        private void FetchHatShells()
        {
            hatShells = new List<MinifigHatSO.Shell>();
            foreach (HatPart hatPart in hatParts) {
                hatShells.Add(new MinifigHatSO.Shell {
                    mesh = hatPart.meshFilter.sharedMesh,
                    material = hatPart.meshRenderer.sharedMaterial
                });
            }
        }

        private void ApplyHat()
        {
            FetchHatLoc();
            if (!HasHatLoc())
                return;
            ApplyHatConfig();
        }

        private void ApplyHatConfig()
        {
            if(HasHatConfig())
                return;
            AdjustHatPartsCountWithHatConfig();
            DisableAllHatParts();
            UpdateAndEnableHatPart();
        }

        private void AdjustHatPartsCountWithHatConfig()
        {
            if (null == config.Hat.Shells)
                return;
            while (config.Hat.Shells.Count > hatParts.Count)
                CreateNewHatPart();
        }

        private bool HasHatConfig()
        {
            return null != config && null != config.Hat;
        }

        private void CreateNewHatPart()
        {
            var go = new GameObject("Shell");
            go.transform.parent = hatLoc;
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
            hatParts.Add(new HatPart
            {
                gameObject = go,
                meshFilter = go.AddComponent<MeshFilter>(),
                meshRenderer = go.AddComponent<MeshRenderer>()
            });
        }

        private void DisableAllHatParts()
        {
            foreach (HatPart hatPart in hatParts)
                hatPart.gameObject.SetActive(false);
        }

        private void UpdateAndEnableHatPart()
        {
            for (int i = 0; i < config.Hat.Shells.Count; i++) {
                hatParts[i].meshFilter.sharedMesh = config.Hat.Shells[i].mesh;
                hatParts[i].meshRenderer.sharedMaterial = config.Hat.Shells[i].material;
                hatParts[i].gameObject.SetActive(true);
            }
        }
        
        private bool HasHatLoc()
        {
            return null != hatLoc;
        }

        private void ApplyMaterial(OutfitPiece[] _pieces, Material _material)
        {
            foreach (OutfitPiece piece in _pieces)
                ApplyMaterial(piece, _material);
        }
    
        private void ApplyMaterial(OutfitPiece _piece, Material _material)
        {
            if (!_material)
                return;
            if (renderers.ContainsKey(_piece))
                renderers[_piece].sharedMaterial = _material;
        }

        private void RecursiveUnHide(Transform transform)
        {
            transform.gameObject.hideFlags = HideFlags.None;
            foreach (Transform child in transform)
            {
                RecursiveUnHide(child);
            }
        }

        [ContextMenu("Update Config")]
        public void UpdateConfig()
        {
            FetchHat();
            config.Hat.Shells = hatShells;
            EditorUtility.SetDirty(config.Hat);
            AssetDatabase.SaveAssetIfDirty(config.Hat);
            
            FetchRenderers();
        
            config.Head.Head = renderers[OutfitPiece.Head]?.sharedMaterial;
            config.Head.Face = renderers[OutfitPiece.Face]?.sharedMaterial;
        
            config.Torso.TorsoMain = renderers[OutfitPiece.Torso_main]?.sharedMaterial;
            config.Torso.TorsoFront = renderers[OutfitPiece.Torso_Front]?.sharedMaterial;
            config.Torso.TorsoBack = renderers[OutfitPiece.Torso_Back]?.sharedMaterial;
            config.Torso.ArmLeft = renderers[OutfitPiece.Arm_L_Front]?.sharedMaterial;
            config.Torso.ArmRight = renderers[OutfitPiece.Arm_R_Front]?.sharedMaterial;
            config.Torso.HandLeft = renderers[OutfitPiece.Hand_Left]?.sharedMaterial;
            config.Torso.HandRight = renderers[OutfitPiece.Hand_Right]?.sharedMaterial;
            config.Torso.ArmsColorSync = armLeft == armRight && handLeft == handRight;
        
            config.Legs.HipMain = renderers[OutfitPiece.Hip_Main]?.sharedMaterial;
            config.Legs.HipFront = renderers[OutfitPiece.Hip_Front]?.sharedMaterial;
            config.Legs.HipCrotch = renderers[OutfitPiece.Hip_Crotch]?.sharedMaterial;
            config.Legs.LegLeftFoot = renderers[OutfitPiece.Leg_L_Foot]?.sharedMaterial;
            config.Legs.LegLeftFront = renderers[OutfitPiece.Leg_L_Front]?.sharedMaterial;
            config.Legs.LegLeftMain = renderers[OutfitPiece.Leg_L_Main]?.sharedMaterial;
            config.Legs.LegLeftSide = renderers[OutfitPiece.Leg_L_Side]?.sharedMaterial;
            config.Legs.LegRightFoot = renderers[OutfitPiece.Leg_R_Foot]?.sharedMaterial;
            config.Legs.LegRightFront = renderers[OutfitPiece.Leg_R_Front]?.sharedMaterial;
            config.Legs.LegRightMain = renderers[OutfitPiece.Leg_R_Main]?.sharedMaterial;
            config.Legs.LegRightSide = renderers[OutfitPiece.Leg_R_Side]?.sharedMaterial;

            EditorUtility.SetDirty(config.Head);
            AssetDatabase.SaveAssetIfDirty(config.Head);
            EditorUtility.SetDirty(config.Torso);
            AssetDatabase.SaveAssetIfDirty(config.Torso);
            EditorUtility.SetDirty(config.Legs);
            AssetDatabase.SaveAssetIfDirty(config.Legs);
            EditorUtility.SetDirty(config);
            AssetDatabase.SaveAssetIfDirty(config);
        }

        [ContextMenu("Apply Config")]
        public void ApplyConfig()
        {
            ApplyHat();
            
            FetchRenderers();
        
            // Head
            ApplyMaterial(OutfitPiece.Face, config.Head.Face);
            ApplyMaterial(OutfitPiece.Head, config.Head.Head);
            // Top
            ApplyMaterial(OutfitPiece.Torso_main, config.Torso.TorsoMain);
            ApplyMaterial(OutfitPiece.Torso_Front, config.Torso.TorsoFront);
            ApplyMaterial(OutfitPiece.Torso_Back, config.Torso.TorsoBack);
            if (config.Torso.ArmLeft) {
                ApplyMaterial(new[]{OutfitPiece.Arm_L_Front, OutfitPiece.Arm_L_Main}, config.Torso.ArmLeft);
                if (config.Torso.ArmsColorSync)
                    ApplyMaterial(new[]{OutfitPiece.Arm_R_Front, OutfitPiece.Arm_R_Main}, config.Torso.ArmLeft);
            }
            if (!config.Torso.ArmsColorSync && config.Torso.ArmRight) {
                ApplyMaterial(new[]{OutfitPiece.Arm_R_Front, OutfitPiece.Arm_R_Main}, config.Torso.ArmRight);
            }
            if (config.Torso.HandLeft) {
                ApplyMaterial(OutfitPiece.Hand_Left, config.Torso.HandLeft);
                if (config.Torso.ArmsColorSync)
                    ApplyMaterial(OutfitPiece.Hand_Right, config.Torso.HandLeft);
            }
            if (!config.Torso.ArmsColorSync && config.Torso.HandRight) {
                ApplyMaterial(OutfitPiece.Hand_Right, config.Torso.HandRight);
            }
            // Bottom
            ApplyMaterial(OutfitPiece.Hip_Main, config.Legs.HipMain);
            ApplyMaterial(OutfitPiece.Hip_Front, config.Legs.HipFront);
            ApplyMaterial(OutfitPiece.Hip_Crotch, config.Legs.HipCrotch);
            ApplyMaterial(OutfitPiece.Leg_L_Foot, config.Legs.LegLeftFoot);
            ApplyMaterial(OutfitPiece.Leg_L_Front, config.Legs.LegLeftFront);
            ApplyMaterial(OutfitPiece.Leg_L_Main, config.Legs.LegLeftMain);
            ApplyMaterial(OutfitPiece.Leg_L_Side, config.Legs.LegLeftSide);
            ApplyMaterial(OutfitPiece.Leg_R_Foot, config.Legs.LegRightFoot);
            ApplyMaterial(OutfitPiece.Leg_R_Front, config.Legs.LegRightFront);
            ApplyMaterial(OutfitPiece.Leg_R_Main, config.Legs.LegRightMain);
            ApplyMaterial(OutfitPiece.Leg_R_Side, config.Legs.LegRightSide);
        }
    }
}
